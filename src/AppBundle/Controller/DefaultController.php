<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use FOS\RestBundle\Controller\Annotations\Post;


class DefaultController extends FOSRestController
{
    /**
     * @Post("/send")
     * 
     * 
     * @RequestParam(name="type", nullable=false, strict=true, description="Email type")
     * @RequestParam(name="email", nullable=false, strict=true, allowBlank=false, description="Email to send")
     * @RequestParam(name="locale", nullable=false, strict=true, allowBlank=false, description="Message language")
     * @RequestParam(name="data", nullable=true, description="Substituions Tags")
     */
    public function sendAction(ParamFetcher $paramFetcher, Request $request)
    {
        $type = $paramFetcher->get('type');
        $email = $paramFetcher->get('email');
        $locale = $paramFetcher->get('locale');
        $data = $paramFetcher->get('data');
        
        $request->setLocale($locale);
        
        $emailService = 'email_' . strtolower($type);
        if( !$this->has($emailService) ){
            throw new BadRequestHttpException(sprintf('Email type not exists: %s', $type));
        }

        /* @var $emailType \AppBundle\Services\EmailTypes\EmailTypeInterface */
        $emailType = $this->get($emailService);
        $mail = $emailType->configure(['email'=>$email, 'locale'=>$locale, 'data'=> $data]);

        # https://sendgrid.com/docs/API_Reference/Web_API_v3/Mail/errors.html
        $response = $emailType->client->mail()->send()->post($mail);
        
        return $response;
    }
}
