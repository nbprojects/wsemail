<?php

namespace AppBundle\Services;

use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

/**
 * Description of SendgridEmail
 *
 */
class SendgridEmail 
{

    private $from;
    private $fromName;
    private $defaultLocale = 'en';
    private $to;
    private $subject;
    private $categories;
    private $substitutions = [];
    public $templates = [];
    public $locale;
    public $mainCategory;
    public $mailPortalName;

    /** @var \SendGrid */
    private $sg;    
    /** @var \SendGrid\Client */
    public $client;
    public $sendgridTemplates;
    
    /**
     * 
     * @param TranslatorInterface $translator
     * @param string $from
     * @param string $fromName
     * @param string $mainCategory
     * @param string $sendgridApiKey
     */
    public function __construct
    (
        TranslatorInterface $translator,
        $from,
        $fromName,
        $mainCategory,
        $mailPortalName,
        $sendgridApiKey,
        $sendgridTemplates
    )
    {
        $this->translator = $translator;
        $this->from = $from;
        $this->fromName = $fromName;
        $this->mainCategory = $mainCategory;
        $this->mailPortalName = $mailPortalName;

        $this->sg = new \SendGrid($sendgridApiKey);
        $this->client = $this->sg->client;
        $this->sendgridTemplates = $sendgridTemplates;
    }
        
    protected function setTo($to) 
    {
        $this->to = $to;
    }
    
    protected function setLocale($locale) 
    {
        $this->locale = $locale;
    }
    
    protected function setSubject($subject)
    {
        $this->subject = $this->translator->trans($subject, array('%mail_portal_name%' => $this->mailPortalName) );
    }
    
    protected function setCategories(array $categories)
    {
        $this->categories = $categories;
    }
    
    protected function setTemplates($templateType)
    {
        if( !isset($this->sendgridTemplates[$templateType]) ){
            throw new ConflictHttpException(sprintf("No template defined for type '%s'. Check the parameters file.", $templateType));
        }
        
        $this->templates = $this->sendgridTemplates[$templateType];
    }
                
    protected function setSubstitutions($data)
    {        
        $this->substitutions = $data; //['-token-'=>[$token]]
    }
        
    protected function getTemplate()
    {
        $templates = $this->templates;
        $locale = $this->getLocale();
        if( !isset($templates[$locale]) ){
            return $templates[$this->defaultLocale];
        }
        
        return $templates[$locale];
    }
    
    protected function getLocale()
    {
        if( isset($this->locale) ){
            return $this->locale;
        }
        
        return $this->defaultLocale;
    }
       
    /**
     * 
     * @return \SendGrid\Mail
     */
    protected function compose()
    {   
        $from = new \SendGrid\Email($this->fromName, $this->from);
        $to = new \SendGrid\Email(null, $this->to);
        
        $mail = new \SendGrid\Mail();
        $mail->setFrom($from);
        $mail->setSubject($this->subject);        
        
        $templateId = $this->getTemplate();
        $mail->setTemplateId($templateId);
        $this->setMailCategories($mail);

        //$mail->personalization[0]->addTo($to);        
        $personalization = new \SendGrid\Personalization();        
        $personalization->addTo($to);
        $this->setPersonalizationSubstitutions($personalization);
        $mail->addPersonalization($personalization);

        return $mail;
    }
    
    private function setMailCategories(\SendGrid\Mail &$mail)
    {
        //$mail->addCategory("May");
        foreach ($this->categories as $category) {
            $mail->addCategory($category);
        }
    }
    
    private function setPersonalizationSubstitutions(\SendGrid\Personalization &$personalization)
    {
        // $personalization->addSubstitution("%name%", "Example User");
        foreach ($this->substitutions as $key => $value) {
            $personalization->addSubstitution($key, $value);
        }
    }
    
    protected function checkOptions($options) 
    {
        $mandatoryOptions = ['email'=>null, 'locale'=>null];
        $optionalOptions = ['data'=>null];
        
        $mandatoryOptionsMissing = array_diff_key($mandatoryOptions, $options);
        if( $mandatoryOptionsMissing ){
            throw new BadRequestHttpException(sprintf('Mandatory option miss: %s', implode(',', $mandatoryOptionsMissing)));
        }
  
        $data = array_merge($optionalOptions, $options);
  
        return $data;
    }
}
