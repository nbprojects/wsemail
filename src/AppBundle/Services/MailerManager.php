<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 21/07/14
 * Time: 15:41
 */

namespace Tools\EmailBundle\Services;

/**
 * Class MailerManager
 * @package Tools\EmaiBundle\Services
 */
class MailerManager
{
    const FROM =  'no-reply@texyon.com';
    const FROM_NAME = 'Texyon Games';
    /** @var \Swift_Mailer  */
    private $mailer;
    /** @var  string */
    private $to;
    /** @var  string */
    private $subject;
    /** @var  string */
    private $body;
    /** @var  string */
    private $template;
    /** @var  bool */
    private $asHtml = true;

    /**
     * @param \Swift_Mailer $mailer
     */
    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param boolean $asHtml
     */
    public function setAsHtml($asHtml)
    {
        $this->asHtml = $asHtml;
    }

    /**
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @param string $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * @param string $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

    /**
     * @return \Swift_Mime_MimePart
     */
    private function getMessage()
    {
        $message = \Swift_Message::newInstance();
        $message->setTo($this->to);
        $message->setSubject($this->subject);
        $message->setFrom(array(self::FROM => self::FROM_NAME));
        $message->setBody($this->body, 'text/plain');

        if (true === $this->asHtml) {
            $message->addPart($this->template, 'text/html');
        }

        return $message;
    }

    /**
     * @return int
     */
    public function send()
    {
        return $this->mailer->send($this->getMessage());
    }
}
