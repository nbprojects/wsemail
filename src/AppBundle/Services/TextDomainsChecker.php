<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 22/07/14
 * Time: 10:07
 */

namespace Tools\EmailBundle\Services;

/**
 * Class TextDomainsChecker
 * @package Tools\EmailBundle\Services
 */
class TextDomainsChecker
{
    /** @var array  */
    private $emailTxtDomains;

    /**
     * @param array $emailTxtDomains
     */
    public function __construct(array $emailTxtDomains)
    {
        $this->emailTxtDomains = $emailTxtDomains;
    }

    /**
     * @param string $emailAddress
     *
     * @return bool
     */
    public function check($emailAddress)
    {
        $domain = substr($emailAddress, strpos($emailAddress, '@')+1);

        if (in_array($domain, $this->emailTxtDomains)) {
            return true;
        }

        return false;
    }
}
