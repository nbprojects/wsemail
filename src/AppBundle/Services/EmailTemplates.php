<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 21/07/14
 * Time: 16:39
 */

namespace Tools\EmailBundle\Services;

use Symfony\Bundle\TwigBundle\TwigEngine;

/**
 * Class EmailTemplates
 * @package Tools\EmaiBundle\Services
 */
class EmailTemplates
{
    /** @var TwigEngine  */
    private $templating;
    /** @var string */
    private $thankYouPageValidation;

    /**
     * @param TwigEngine $timedTwigEngine
     * @param string     $thankYouPageValidation
     */
    public function __construct(TwigEngine $timedTwigEngine, $thankYouPageValidation)
    {
        $this->templating = $timedTwigEngine;
        $this->thankYouPageValidation = $thankYouPageValidation;
    }

    /**
     * @param string  $token
     * @param boolean $is_TLC_userAgent
     *
     * @return array
     */
    public function getSecurityEmailTemplates($token, $is_TLC_userAgent)
    {
        $options = array(
            'authtoken' => $token,
            'title'     => 'EMAIL_VALIDATIE_ACCOUNT_TIT',
            'action'    => 'EMAIL_VALIDATIE_ACCOUNT_ACTION',
            'text'      => 'EMAIL_VALIDATIE_ACCOUNT_PARAGRAPH',
            'textExtra' => 'EMAIL_VALIDATIE_ACCOUNT_PARAGRAPH_EXTRA_TLC'
        );

        if ($is_TLC_userAgent) {
            return array(
                'html' => $this->templating->render('ToolsEmailBundle:Security:securityEmail_TLC.html.twig', $options),
                'text' => $this->templating->render('ToolsEmailBundle:Security:securityEmail_TLC.txt.twig', $options)
            );
        } 
        
        return array(
            'html' => $this->templating->render('ToolsEmailBundle:Security:securityEmail.html.twig', $options),
            'text' => $this->templating->render('ToolsEmailBundle:Security:securityEmail.txt.twig', $options)
        );
    }

    /**
     * @param string $UEIgame
     *
     * @return array
     */
    public function getTlcDownloadEmailTemplates($UEIgame)
    {
        //$vars_link = ($UEIgame !== null)? "#window:downloadClient;game:{$UEIgame}" : "#window:downloadClient";
        $vars_link = $this->thankYouPageValidation;
                
        $options = array(
            'title'         => 'EMAIL_TLC_DOWNLOAD_TIT',
            'action'        => 'EMAIL_TLC_DOWNLOAD_ACTION',
            'text1'         => 'EMAIL_TLC_DOWNLOAD_PARAGRAPH_1',
            'text2'         => 'EMAIL_TLC_DOWNLOAD_PARAGRAPH_2',
            'text3a'        => 'EMAIL_TLC_DOWNLOAD_PARAGRAPH_3_LIST_1',
            'text3b'        => 'EMAIL_TLC_DOWNLOAD_PARAGRAPH_3_LIST_2',
            'text3c'        => 'EMAIL_TLC_DOWNLOAD_PARAGRAPH_3_LIST_3',
            'vars_link'     => $vars_link
        );
        
        return array(
            'html' => $this->templating->render('ToolsEmailBundle:Security:tlcDownloadEmail.html.twig', $options),
            'text' => $this->templating->render('ToolsEmailBundle:Security:tlcDownloadEmail.txt.twig', $options)
        );
    }

    /**
     * @param string $email
     * @param string $plainPass
     * @param string $channel
     *
     * @return array
     */
    public function getSocialRegisterEmailTemplate($email, $plainPass, $channel)
    {
        $options = array(
            'password'  => $plainPass,
            'email'     => $email,
            'title'     => 'EMAIL_VALIDATIE_ACCOUNT_TIT',
            'action'    => '',
            'text1'     => 'EMAIL_REGISTER_SOCIAL_PARAGRAPH_'. strtoupper($channel->getCodchannel()) . '_1',
            'text2'     => 'EMAIL_REGISTER_SOCIAL_PARAGRAPH_'. strtoupper($channel->getCodchannel()) . '_2',
            'text3'     => 'EMAIL_REGISTER_SOCIAL_PARAGRAPH_3'
        );

        return array(
            'html' => $this->templating->render('@ToolsEmail/Security/registerSocialEmail.html.twig', $options),
            'text' => $this->templating->render('@ToolsEmail/Security/registerSocialEmail.txt.twig', $options)
        );
    }

    /**
     * @param string $token
     *
     * @return array
     */
    public function getChangePasswordEmailTemplates($token)
    {
        $options = array(
            'authtoken' => $token,
            'title'     => 'RECOVERY_PASSWORD_TIT',
            'action'    => 'RECOVERY_PASSWORD_ACTION',
            'text'      => 'RECOVERY_PASSWORD_PARAGRAPH'
        );

        return array(
            'html' => $this->templating->render('ToolsEmailBundle:Security:securityEmail.html.twig', $options),
            'text' => $this->templating->render('ToolsEmailBundle:Security:securityEmail.txt.twig', $options)
        );
    }

    /**
     * @param string $token
     *
     * @return array
     */
    public function getChangeBirthdateEmailTemplates($token)
    {
        $options = array(
            'authtoken' => $token,
            'title'     => 'CHANGE_BIRTHDATE_TIT',
            'action'    => 'CHANGE_BIRTHDATE_ACTION',
            'text'      => 'CHANGE_BIRTHDATE_PARAGRAPH'
        );

        return array(
            'html' => $this->templating->render('ToolsEmailBundle:Security:securityEmail.html.twig', $options),
            'text' => $this->templating->render('ToolsEmailBundle:Security:securityEmail.txt.twig', $options)
        );
    }

    /**
     * @param string  $token
     * @param boolean $is_TLC_userAgent
     *
     * @return array
     */
    public function getForgotEmailTemplates($token, $is_TLC_userAgent)
    {
        $options = array(
            'authtoken' => $token,
            'title'     => 'FORGOT_PASSWORD_TIT',
            'action'    => 'FORGOT_PASSWORD_ACTION',
            'text'      => 'FORGOT_PASSWORD_PARAGRAPH',
            'textExtra' => 'EMAIL_VALIDATIE_ACCOUNT_PARAGRAPH_EXTRA_TLC'
        );

        if ($is_TLC_userAgent) {
            return array(
                'html' => $this->templating->render('ToolsEmailBundle:Security:securityEmail_TLC.html.twig', $options),
                'text' => $this->templating->render('ToolsEmailBundle:Security:securityEmail_TLC.txt.twig', $options)
            );
        } 
        
        return array(
            'html' => $this->templating->render('ToolsEmailBundle:Security:securityEmail.html.twig', $options),
            'text' => $this->templating->render('ToolsEmailBundle:Security:securityEmail.txt.twig', $options)
        );
    }

    /**
     * @param string $token
     *
     * @return array
     */
    public function getUnlockAccountEmailTemplates($token)
    {
        $options = array(
            'password'  => $token,
            'title'     => 'UNLOCK_ACCOUNT_TIT',
            'action'    => 'UNLOCK_ACCOUNT_ACTION',
            'text'      => 'UNLOCK_ACCOUNT_PARAGRAPH'
        );
        
     
        return array(
            'html' => $this->templating->render('ToolsEmailBundle:Security:tokenEmail.html.twig', $options),
            'text' => $this->templating->render('ToolsEmailBundle:Security:tokenEmail.txt.twig', $options)
        );
    }

    /**
     * @param array  $deliveredCoupons
     * @param array  $games
     * @param string $url
     * 
     * @return array
     */
    public function getNotificationPortalCouponEmailTemplates($deliveredCoupons, $games, $url)
    {
        
        $options = array(
            'title'       => 'NOTIFICATION_PORTAL_COUPON_TIT',
            'action'      => '',
            'text'        => 'NOTIFICATION_PORTAL_COUPON_PARAGRAPH',
            'text_gei'    => 'NOTIFICATION_PORTAL_COUPON_PARAGRAPH_GEI',
            'text_tei'    => 'NOTIFICATION_PORTAL_COUPON_PARAGRAPH_TEI',
            'display_gei' => $deliveredCoupons['GEI'],
            'display_tei' => $deliveredCoupons['TEI'],
            'games'       => $games,
            'url'         => $url
        );
        
     
        return array(
            'html' => $this->templating->render('ToolsEmailBundle:Notification:portalCouponEmail.html.twig', $options),
            'text' => $this->templating->render('ToolsEmailBundle:Notification:portalCouponEmail.txt.twig', $options)
        );
    }
}
