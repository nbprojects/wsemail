<?php

namespace AppBundle\Services\EmailTypes;

use AppBundle\Services\SendgridEmail;
use AppBundle\Services\EmailTypes\EmailTypeInterface;

/**
 * Description of UnlockAccountEmail
 *
 */
class UnlockAccountEmail extends SendgridEmail implements EmailTypeInterface
{
    const SUBJECT = 'SUBJECT_UNLOCK_ACCOUNT';
    const CATEGORY = 'unlock_account';
    const TEMPLATES = 'unlock_account'; 
    
    /**
     * 
     * @param array $options
     * @return \SendGrid\Email
     */
    public function configure(array $options)
    {
        $data = $this->checkOptions($options);

        $this->setTo($data['email']);        
        $this->setLocale($data['locale']);        
        $this->setSubstitutions($data['data']);
   
        $this->setTemplates(self::TEMPLATES);
        $this->setSubject(self::SUBJECT);
        $this->setCategories([$this->mainCategory, self::CATEGORY, $this->locale]);
        
        return $this->compose();
    }
    


}
