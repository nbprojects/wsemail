<?php

namespace AppBundle\Services\EmailTypes;

use AppBundle\Services\SendgridEmail;
use AppBundle\Services\EmailTypes\EmailTypeInterface;

/**
 * Description of ValidatedAccountEmail
 *
 */
class ValidatedAccountEmail extends SendgridEmail implements EmailTypeInterface
{
    const SUBJECT = 'SUBJECT_VALIDATED_ACCOUNT';
    const CATEGORY = 'validated';
    const TEMPLATES = 'validated_account'; //browser_validated
    
    /**
     * 
     * @param array $options
     * @return \SendGrid\Email
     */
    public function configure(array $options)
    {
        $data = $this->checkOptions($options);

        $this->setTo($data['email']);        
        $this->setLocale($data['locale']);        
        $this->setSubstitutions($data['data']);
   
        $this->setTemplates(self::TEMPLATES);
        $this->setSubject(self::SUBJECT);
        $this->setCategories([$this->mainCategory, self::CATEGORY, $this->locale]);
        
        return $this->compose();
    }
    
    
    private function getSubstitutionsAllowed()
    {
        return [   
            '-tlc_download-',
            '-email-',
            '-nick-',
            '-date-',
            '-recover-',
            '-soporte-'
        ];
    }


}
