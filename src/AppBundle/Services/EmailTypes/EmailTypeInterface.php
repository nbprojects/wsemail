<?php

namespace AppBundle\Services\EmailTypes;

/**
 *
 * @author NachoBordon
 */
interface EmailTypeInterface 
{
    /**
     * @return \SendGrid\Email
     */
    public function configure(array $options);

}
