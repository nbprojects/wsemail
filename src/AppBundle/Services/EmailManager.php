<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 22/07/14
 * Time: 9:46
 */

namespace Tools\EmailBundle\Services;

use Symfony\Component\Translation\TranslatorInterface;
/**
 * Class EmailManager
 * @package Tools\EmailBundle\Services
 */
class EmailManager
{
    const TEXYON_GAMES_REGISTER = 'TEXYON_GAMES_REGISTER';
    const TEXYON_GAMES_RECOVERY_PASSWORD = 'TEXYON_GAMES_RECOVERY_PASSWORD';
    const TEXYON_CHANGE_BIRTHDATE = 'TEXYON_CHANGE_BIRTHDATE';
    const TEXYON_TLC_DOWNLOAD = 'TEXYON_TLC_DOWNLOAD';
    const TEXYON_NOTIFICATION_PORTAL_COUPON = 'TEXYON_NOTIFICATION_PORTAL_COUPON';
    /** @var \Tools\EmailBundle\Services\MailerManager  */
    private $mailerManager;
    /** @var \Tools\EmailBundle\Services\EmailTemplates  */
    private $emailTemplates;
    /** @var \Tools\EmailBundle\Services\TextDomainsChecker  */
    private $textDomainsChecker;
    /** @var TranslatorInterface  */
    private $translator;
    /** @var  string */
    private $to;
    /** @var  string */
    private $subject;
    /** @var  string */
    private $template;
    /** @var  string */
    private $plain;

    /**
     * @param EmailTemplates     $emailTemplates
     * @param MailerManager      $mailerManager
     * @param TextDomainsChecker $textDomainsChecker
     * @param Translator         $translator
     */
    public function __construct
    (
        EmailTemplates $emailTemplates,
        MailerManager $mailerManager,
        TextDomainsChecker $textDomainsChecker,
        TranslatorInterface $translator
    )
    {
        $this->mailerManager = $mailerManager;
        $this->emailTemplates = $emailTemplates;
        $this->textDomainsChecker = $textDomainsChecker;
        $this->translator = $translator;
    }

    /**
     * @param string $to
     * @param string $subject
     * @param array  $templates
     */
    private function setParams($to, $subject, array $templates)
    {
        $this->to = $to;
        $this->subject = $subject;
        $this->template = $templates['html'];
        $this->plain = $templates['text'];
    }

    /**
     * @return int
     */
    private function send()
    {
        $this->compose();

        return $this->mailerManager->send();
    }

    /**
     * @param string $text
     *
     * @return string
     */
    private function getTranslation($text)
    {
        return $this->translator->trans($text);
    }

    /**
     * @param string  $to
     * @param string  $token
     * @param boolean $is_TLC_userAgent
     *
     * @return int
     */
    public function sendRegisterMail($to, $token, $is_TLC_userAgent)
    {
        $subject = $this->getTranslation(self::TEXYON_GAMES_REGISTER);
        $templates= $this->emailTemplates->getSecurityEmailTemplates($token, $is_TLC_userAgent);

        $this->setParams($to, $subject, $templates);

        return $this->send();
    }

    /**
     * @param string $to
     * @param string $UEIgame
     *
     * @return int
     */
    public function sendTlcDownloadMail($to, $UEIgame)
    {
        $subject = $this->getTranslation(self::TEXYON_TLC_DOWNLOAD);
        $templates= $this->emailTemplates->getTlcDownloadEmailTemplates($UEIgame);

        $this->setParams($to, $subject, $templates);

        return $this->send();
    }

    private function compose()
    {
        $this->mailerManager->setTo($this->to);
        $this->mailerManager->setSubject($this->subject);
        $this->mailerManager->setBody($this->plain);
        $this->mailerManager->setTemplate($this->template);

        if (true === $this->textDomainsChecker->check($this->to)) {
            $this->mailerManager->setAsHtml(false);
        }
    }

    /**
     * @param string $to
     * @param string $plainPass
     * @param string $channel
     *
     * @return int
     */
    public function sendSocialRegisterMail($to, $plainPass, $channel)
    {
        $templates= $this->emailTemplates->getSocialRegisterEmailTemplate($to, $plainPass, $channel);
        $subject = $this->getTranslation(self::TEXYON_GAMES_REGISTER);

        $this->setParams($to, $subject, $templates);

        return $this->send();

    }

    /**
     * @param string $to
     * @param string $token
     *
     * @return int
     */
    public function sendChangePasswordMail($to, $token)
    {
        $templates = $this->emailTemplates->getChangePasswordEmailTemplates($token);
        $subject = $this->getTranslation(self::TEXYON_GAMES_RECOVERY_PASSWORD);

        $this->setParams($to, $subject, $templates);

        return $this->send();
    }

    /**
     * @param string $to
     * @param string $token
     *
     * @return int
     */
    public function sendChangeBirthdateMail($to, $token)
    {
        $templates = $this->emailTemplates->getChangeBirthdateEmailTemplates($token);
        $subject = $this->getTranslation(self::TEXYON_CHANGE_BIRTHDATE);

        $this->setParams($to, $subject, $templates);

        return $this->send();
    }

    /**
     * @param string $to
     * @param string $token
     * @param boolean $is_TLC_userAgent
     *
     * @return int
     */
    public function sendForgotMail($to, $token, $is_TLC_userAgent)
    {
        $templates = $this->emailTemplates->getForgotEmailTemplates($token, $is_TLC_userAgent);
        $subject = $this->getTranslation('TEXYON_GAMES_RECOVERY_PASSWORD');

        $this->setParams($to, $subject, $templates);

        return $this->send();
    }

    /**
     * @param string $to
     * @param string $token
     *
     * @return int
     */
    public function sendUnlockAccountMail($to, $token)
    {
        $templates = $this->emailTemplates->getUnlockAccountEmailTemplates($token); 
        $subject = $this->getTranslation('TEXYON_GAMES_UNLOCK_ACCOUNT');

        $this->setParams($to, $subject, $templates);

        return $this->send();
    }
    
    /**
     * @param string  $to
     * @param array   $deliveredCoupons
     * @param array   $games
     * @param string  $url
     *
     * @return int
     */
    public function sendNotificationPortalCouponMail($to, $deliveredCoupons, $games, $url)
    {
        $subject = $this->getTranslation(self::TEXYON_NOTIFICATION_PORTAL_COUPON);
        $templates= $this->emailTemplates->getNotificationPortalCouponEmailTemplates($deliveredCoupons, $games, $url);

        $this->setParams($to, $subject, $templates);

        return $this->send();
    }
}
