<?php

namespace Tools\EmailBundle\Services;

use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SendgridEmailManager
 * @package Tools\EmailBundle\Services
 */
class SendgridEmailManager
{
    const FROM = 'no-reply@mx.texyon.com';
    const FROM_NAME = 'Texyon Games';
    const TEXYON_GAMES_REGISTER = 'TEXYON_GAMES_REGISTER';
    const TEXYON_GAMES_RECOVERY_PASSWORD = 'TEXYON_GAMES_RECOVERY_PASSWORD';
    const TEXYON_CHANGE_BIRTHDATE = 'TEXYON_CHANGE_BIRTHDATE';
    const TEXYON_TLC_DOWNLOAD = 'TEXYON_TLC_DOWNLOAD';
    const TEXYON_WELCOME = 'TEXYON_WELCOME';
    const TEXYON_NOTIFICATION_PORTAL_COUPON = 'TEXYON_NOTIFICATION_PORTAL_COUPON';
    const SENDGRID_MAIN_CATEGORY = 'portal';
    
    /** @var \Tools\EmailBundle\Services\MailerManager  */
    private $mailerManager;
    /** @var \Tools\EmailBundle\Services\EmailTemplates  */
    private $emailTemplates;
    /** @var \Tools\EmailBundle\Services\TextDomainsChecker  */
    private $textDomainsChecker;
    /** @var TranslatorInterface  */
    private $translator;   
    /** @var string */
    private $sendgrid_username;
    /** @var string */
    private $sendgrid_password;
    /** @var \SendGrid */
    private $sendgrid;    
    /** @var string */
    private $supportUrl;
    
    /** @var  array */
    private $to;
    /** @var  string */
    private $subject;
    /** @var  string */
    private $substitutions;
    /** @var  array */
    private $categories;
    

    /**
     * @param EmailTemplates     $emailTemplates
     * @param MailerManager      $mailerManager
     * @param TextDomainsChecker $textDomainsChecker
     * @param Translator         $translator
     * @param string             $sendgrid_username
     * @param string             $sendgrid_password
     * @param string             $supportUrl
     */
    public function __construct
    (
        EmailTemplates $emailTemplates,
        MailerManager $mailerManager,
        TextDomainsChecker $textDomainsChecker,
        TranslatorInterface $translator,
        $sendgrid_username,
        $sendgrid_password,
        $supportUrl
    )
    {
        $this->mailerManager = $mailerManager;
        $this->emailTemplates = $emailTemplates;
        $this->textDomainsChecker = $textDomainsChecker;
        $this->translator = $translator;
        $this->supportUrl = $supportUrl;
        
        $this->sendgrid_username = $sendgrid_username;
        $this->sendgrid_password = $sendgrid_password;
        $this->sendgrid = new \SendGrid($this->sendgrid_username, $this->sendgrid_password);
    }
 
    /**
     * 
     * @param string $referer
     * @param string $token
     * @param string $UEIgame
     * @return string
     */
    private function getUrlValidateToken($referer, $token, $UEIgame=NULL) 
    {
        $url = "{$referer}#validate:{$token}";
                
        if ($UEIgame !== null) {
            $url .= ";game:{$UEIgame}";
        }
        
        return $url; 
    }
 
    /**
     * 
     * @param string $referer
     * @param string $token
     * @param string $UEIgame
     * @return string
     */
    private function getUrlBrowserValidateToken($referer, $token, $UEIgame=NULL) 
    {
        $url = "{$referer}#validatebr:{$token}";
                        
        if ($UEIgame !== null) {
            $url .= ";game:{$UEIgame}";
        }
        
        return $url; 
    }
    
    /**
     * 
     * @param string $token
     * @return string
     */
    private function getUrlTlcToken($token) 
    { 
        return "tx://{$token}";     
    }

    /**
     * 
     * @param string $referer
     * @param string $UEIgame
     * @return string
     */
    private function getWindowDownloadClient($referer, $UEIgame=NULL)
    {
        $url = "{$referer}#window:downloadClient";
        
        if ($UEIgame !== null) {
            $url .= ";game:{$UEIgame}";
        }
        
        return $url;
    }
    
    /**
     * 
     * @param string $token
     * @param string $referer
     * @return array
     */
    private function getSubstitutions($token, $referer)
    {        
        $url = $this->getUrlValidateToken($referer, $token);
        $url_tlc = $this->getUrlTlcToken($token);

        $substitutions['-token-']   = [$token];       
        $substitutions['-url-']     = [$url];
        $substitutions['-url_tlc-'] = [$url_tlc];

        return $substitutions;
    }
    
    /**
     * @param string $to
     * @param string $subject
     * @param string $substitutions
     * @param string $template_id
     * @param array  $categories
     */
    private function setParams($to, $subject, $substitutions, $template_id, $categories=array())
    {
        $this->to = array($to);
        $this->subject = $subject;    
        $this->substitutions = $substitutions;
        $this->template_id = $template_id;
        $this->categories = $categories;
    }

    /**
     * @return int
     */
    private function send()
    {
        $email = $this->compose();

        return $this->sendgrid->send($email);
    }

    /**
     * @param string $text
     *
     * @return string
     */
    private function getTranslation($text)
    {
        return $this->translator->trans($text);
    }

    /**
     * 
     * @return \SendGrid\Email
     */
    private function compose()
    {        
        $email = new \SendGrid\Email();
        
        $email
            ->setFrom(self::FROM)
            ->setFromName(self::FROM_NAME)
            ->setSubject($this->subject)
            ->setSmtpapiTos($this->to)
            ->setSubstitutions($this->substitutions)
            ->setHtml(' ') // si no se pasa vacio, sendgrid lo setea en la etiqueta <%body%> del template
            ->setText(' ') // si no se pasa vacio, sendgrid lo setea en la etiqueta <%body%> del template
            ->setTemplateId($this->template_id)
            ->setCategories($this->categories)
        ;
        
        return $email;
    }
    
    
    /**
     * @param string  $to
     * @param string  $token
     * @param boolean $is_TLC_userAgent
     * @param string  $referer
     * @param string  $locale
     *
     * @return int
     */
    public function sendRegisterMail($to, $token, $is_TLC_userAgent, $referer, $locale)
    {
        $subject = $this->getTranslation(self::TEXYON_GAMES_REGISTER);
        $templates = [
            'es' => 'f2f5904d-4982-47d5-ae98-59bfed02b3ab',
            'en' => '2e89a324-1396-4f41-93a2-086d760ca6fb',
            'pt' => 'd536ffa7-352a-43dd-bf2a-2996540e4856',
            'tr' => '2e89a324-1396-4f41-93a2-086d760ca6fb'            
        ];
        
        $authtoken = $this->getUrlValidateToken($referer, $token);
        $substitutions = ['-token-'=>[$authtoken]];
        $categories = [self::SENDGRID_MAIN_CATEGORY, 'validation', $locale];
    
        $this->setParams($to, $subject, $substitutions, $templates[$locale], $categories);

        return $this->send();
    }
    
    
    public function sendRegisterBrowserGameMail($to, $uniqueNick, $birthDate, $token, $UEIgame, $urlPlaypage, $locale)
    {
        $subject = $this->getTranslation(self::TEXYON_GAMES_REGISTER);
        $templates = [
            'es' => '84dd78ed-2eb8-44ed-969f-3637c2fa6518',
            'en' => '9987c634-d1c0-4970-9c57-50d7ad41a916',
            'pt' => 'ce84a688-12c6-4fb0-9ef7-b07084b773b6',
            'tr' => 'a473cd52-7721-4cf7-b9b5-068f55d4fcdf'
        ];
        
        $authtoken = $this->getUrlBrowserValidateToken($urlPlaypage, $token, $UEIgame);
        $substitutions = [
            '-token-'=>[$authtoken],
            '-email-'=>[$to],
            '-uniqueNick-'=>[$uniqueNick],
            '-birthDate-'=>[$birthDate->format('d-m-Y')]
        ];
        $categories = [self::SENDGRID_MAIN_CATEGORY, 'validation', $locale];
    
        $this->setParams($to, $subject, $substitutions, $templates[$locale], $categories);

        return $this->send();
    }
    
        /**
     * @param string  $to
     * @param string  $token
     * @param string  $referer
     * @param string  $locale
     *
     * @return int
     */
    public function sendRegisterTlcMail($to, $token, $referer, $locale)
    {
        $subject = $this->getTranslation(self::TEXYON_GAMES_REGISTER);
        $templates = [
            'es' => '9ed1ec73-3aa3-461f-a670-bdedadbc69d0',
            'en' => 'e985d125-7125-45a7-9d83-15f005a33e20',
            'pt' => '7f28cec0-bb10-4127-88b8-71775a9b0056'
        ];
        
        $substitutions = $this->getSubstitutions($token, $referer);
        $categories = [self::SENDGRID_MAIN_CATEGORY, 'validation', $locale];
    
        $this->setParams($to, $subject, $substitutions, $templates[$locale], $categories);

        return $this->send();
    }

    
    /**
     * @param string $to
     * @param string $plainPass
     * @param string $channel
     * @param string $locale
     *
     * @return int
     */
    public function sendSocialRegisterMail($to, $plainPass, $channel, $locale)
    {
        $subject = $this->getTranslation(self::TEXYON_GAMES_REGISTER);
        $templates = [
            'es' => 'f1299325-5e3c-4e20-9fb6-39398b7fd0a8',
            'en' => '28c46629-c6ec-4dc0-88e1-b7049e177786',
            'pt' => 'ed338f00-c3ce-4cfc-8f1c-ad4489f5b21d'
        ];
        
        switch (strtoupper($channel)) {
            case 'FB': $channel_name = 'Facebook'; break;
            case 'GP': $channel_name = 'Google plus'; break;
            case 'ST': $channel_name = 'Steam'; break;            
            default:  $channel_name = 'Texyon'; break;
        }
        
        $substitutions = ['-email-'=>[$to], '-password-'=>[$plainPass], '-channel-'=>[$channel_name]];
        $categories = [self::SENDGRID_MAIN_CATEGORY, 'register_social', $locale];
        
        $this->setParams($to, $subject, $substitutions, $templates[$locale], $categories);

        return $this->send();

    }
    
    /**
     * Browser games Email validation
     * 
     * @param string $to
     * @param string $UEIgame
     * @param string $locale
     * @param string $referer
     *
     * @return int
     */
    public function sendWelcomeMail($to, $nickname, $birthdate, $UEIgame, $locale, $referer)
    {
        $subject = $this->getTranslation(self::TEXYON_WELCOME);
        $templates = [
            'es' => '5c0cbefd-7cac-4c42-8b94-0334082104a7',
            'en' => '6a9c4408-adb2-41ae-a18a-d20c5a0a5bff',
            'pt' => '6c0fbf02-8760-44c3-87d7-ec74ecae50b0',
            'tr' => '92fdfa80-4aeb-42ca-8a5a-83828d7bcd76'
        ];
        
//        if($UEIgame !== null) {
//            $authtoken = $this->getUrlBrowserValidateToken($referer, $token, $UEIgame);
//        }else{
            $authtoken = $this->getWindowDownloadClient($referer, $UEIgame); 
        //}
        
        $substitutions = [   
            '-tlc_download-' => [$authtoken],
            '-email-'     => [$to],
            '-nick-'      => [$nickname],            
            '-date-'      => [$birthdate->format('d-m-Y')],
            '-recover-'   => ["{$referer}#window:forgotform"], 
            '-soporte-'   => [$this->supportUrl]
        ];
        $categories = [self::SENDGRID_MAIN_CATEGORY, 'browser_validated', $locale];
        $this->setParams($to, $subject, $substitutions, $templates[$locale], $categories);

        return $this->send();
    }
    /**
     * @param string $to
     * @param string $UEIgame
     * @param string $locale
     * @param string $referer
     *
     * @return int
     */
    public function sendTlcDownloadMail($to, $UEIgame, $locale, $referer)
    {
        $subject = $this->getTranslation(self::TEXYON_TLC_DOWNLOAD);
        $templates = [
            'es' => '81416822-8528-4c5d-8ab9-853c3cc446d5',
            'en' => '5da6dbf9-546a-43e8-bc73-b8ef0d9b6ba8',
            'pt' => '9f289802-bd44-4282-baa7-3e84dba2172e'
        ];
        
        $vars_link = $this->getWindowDownloadClient($referer, $UEIgame);
        $substitutions = ['-vars_link-'=>[$vars_link]];
        $categories = [self::SENDGRID_MAIN_CATEGORY, 'tlc_download', $locale];
        
        $this->setParams($to, $subject, $substitutions, $templates[$locale], $categories);

        return $this->send();
    }
    
    /**
     * @param string $to
     * @param string $token
     * @param string $referer
     * @param string $locale
     *
     * @return int
     */
    public function sendChangePasswordMail($to, $token, $referer, $locale)
    {
        $subject = $this->getTranslation(self::TEXYON_GAMES_RECOVERY_PASSWORD);
        $templates = [
            'es' => 'bd01eccf-389d-44bf-a3b1-3e07bcecc2e0',
            'en' => '0e1d5684-79c6-4672-9c5e-5aaab42ee31a',
            'pt' => '29f978d0-3c55-4e86-bb38-b32a343b5d38'
        ];
        $token_url = $this->getUrlValidateToken($referer, $token);
        $substitutions = ['-token-'=>[$token_url]];
        $categories = [self::SENDGRID_MAIN_CATEGORY, 'change_password', $locale];
        
        $this->setParams($to, $subject, $substitutions, $templates[$locale], $categories);

        return $this->send();
    }

    /**
     * @param string $to
     * @param string $token
     *
     * @return int
     */
    public function sendChangeBirthdateMail($to, $token, $referer, $locale)
    {
        $subject = $this->getTranslation(self::TEXYON_CHANGE_BIRTHDATE);
        $templates = [
            'es' => '4db753bb-efb4-47ec-9485-513f5fb57228',
            'en' => 'c6b36094-0ba0-447b-99a7-b8bfe6dca6af',
            'pt' => 'df03f930-aae6-4983-821a-9af2bcfdeb74'
        ];
        $token_url = $this->getUrlValidateToken($referer, $token);
        $substitutions = ['-token-'=>[$token_url]];
        $categories = [self::SENDGRID_MAIN_CATEGORY, 'change_birthdate', $locale];
        
        $this->setParams($to, $subject, $substitutions, $templates[$locale], $categories);

        return $this->send();
    }

    /**
     * @param string    $to
     * @param string    $token
     * @param boolean   $is_TLC_userAgent
     * @param string    $referer
     * @param string    $locale
     *
     * @return int
     */
    public function sendForgotMail($to, $token, $is_TLC_userAgent, $referer, $locale)
    {
        $subject = $this->getTranslation('TEXYON_GAMES_RECOVERY_PASSWORD');
                
        $templates = [
            'es' => '302426ba-c165-4fdd-916e-0c4eb1feac05',
            'en' => '6133185e-d482-4b3a-8c7c-ce675110d6b7',
            'pt' => '5b8c2201-dd34-4302-a144-0bd11865242b'
        ];
                
        $substitutions = $this->getSubstitutions($token, $referer);
        $categories = [self::SENDGRID_MAIN_CATEGORY, 'forgot_password', $locale];
    
        $this->setParams($to, $subject, $substitutions, $templates[$locale], $categories);

        return $this->send();
    }

    /**
     * @param string $to
     * @param string $token
     * @param string $locale
     *
     * @return int
     */
    public function sendUnlockAccountMail($to, $token, $locale)
    {
        $subject = $this->getTranslation('TEXYON_GAMES_UNLOCK_ACCOUNT');
        $templates = [
            'es' => '633be3a8-436d-4a29-b310-1b4366882479',
            'en' => '75eb7a4e-c26e-496c-a635-2da4c720c992',
            'pt' => 'db7fa5bf-36f7-4fe7-b4d4-c4be9b049fcd'
        ];
        $substitutions = ['-token-'=>[$token]];
        $categories = [self::SENDGRID_MAIN_CATEGORY, 'unlock_account', $locale];
        
        $this->setParams($to, $subject, $substitutions, $templates[$locale], $categories);

        return $this->send();
    }
    
    /**
     * @param string  $to
     * @param array   $deliveredCoupons
     * @param array   $games
     * @param string  $url
     * @param string  $locale
     *
     * @return int
     */
    public function sendNotificationPortalCouponMail($to, $deliveredCoupons, $games, $url, $locale)
    {
        $subject = $this->getTranslation(self::TEXYON_NOTIFICATION_PORTAL_COUPON);
        $templates = [
            'es' => 'fa41ccec-e260-4e06-97d7-11b59ef1fa7a',
            'en' => 'd8794738-0db3-4ba0-8a52-b32ca26a6eee',
            'pt' => 'b5115fa5-d3ab-4fc5-bfae-ce99772cf308'
        ];
        
//        $deliveredCoupons['GEI']
//        $deliveredCoupons['TEI']
        if(is_array($games)) { $games = implode(",", $games); }
        $substitutions = ['-games-' => [$games], '-url-' => [$url]];
        $categories = [self::SENDGRID_MAIN_CATEGORY, 'coupon_redeemed', $locale];
    
        $this->setParams($to, $subject, $substitutions, $templates[$locale], $categories);

        return $this->send();
    }
}
